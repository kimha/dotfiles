#
# Basic settings
#

# History
HISTFILE=~/.histfile
HISTSIZE=20000
SAVEHIST=20000
export HISTIGNORE='pwd:exit:clear:ls:l:ll:la'

setopt extendedglob
setopt nobeep
bindkey -v '^?' backward-delete-char # Vim backspace over Vi backspace
unsetopt autocd notify
zstyle :compinstall filename '/home/hk/.zshrc'

autoload -Uz compinit
compinit
setopt COMPLETE_ALIASES
PROMPT='[%F{yellow}%n%f@%F{cyan}%m%f %F{blue}%B%1~%b%f] $ '


# PATH

# Source other files
[ -f $HOME/.config/zsh/aliases ] && source $HOME/.config/zsh/aliases

#  fzf
[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh

export FZF_DEFAULT_OPTS="
  --layout=reverse
  --bind '?:toggle-preview'
"

export FZF_BASE_COMMAND='fd --no-ignore --hidden --follow --ignore-file /usr/share/fzf/fzfignore'
export FZF_DEFAULT_COMMAND="$FZF_BASE_COMMAND -t f -t l"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="$FZF_BASE_COMMAND -t d"

__fzf_compgen_path() {
  fd -H . "$1"
}

__fzf_compgen_dir() {
  fd --type d -H . "$1"
}

vim-fzf-widget() {
  files=$(eval $FZF_CTRL_T_COMMAND | fzf -m) && nvim $files
  zle reset-prompt
}
zle     -N    vim-fzf-widget
bindkey "^[e" vim-fzf-widget

sudoedit-fzf-widget() {
  files=$(eval $FZF_CTRL_T_COMMAND | fzf -m) && sudoedit $files
  zle reset-prompt
}
zle     -N    sudoedit-fzf-widget
bindkey "^[w" sudoedit-fzf-widget

xdg-fzf-widget() {
  files=$(eval $FZF_CTRL_T_COMMAND | fzf) && xdg-open $files & disown
  zle reset-prompt
}
zle     -N    xdg-fzf-widget
bindkey "^[x" xdg-fzf-widget


# Functions
function stopwatch(){
  date1=`date +%s`; 
   while true; do 
    echo -ne "$(date -u --date @$((`date +%s` - $date1)) +%H:%M:%S)\r"; 
    sleep 0.1
   done
}

function chtsh() {
  curl "cht.sh/$1"
}

# new home
export AHOME=$HOME/home
alias cd='HOME=$HOME/home cd'
cd ~/home


#
## >>> conda initialize >>>
## !! Contents within this block are managed by 'conda init' !!
#__conda_setup="$('/opt/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
#if [ $? -eq 0 ]; then
#    eval "$__conda_setup"
#else
#    if [ -f "/opt/miniconda3/etc/profile.d/conda.sh" ]; then
#        . "/opt/miniconda3/etc/profile.d/conda.sh"
#    else
#        export PATH="/opt/miniconda3/bin:$PATH"
#    fi
#fi
#unset __conda_setup
## <<< conda initialize <<<
#
ibus-daemon -drx


##
##
### The next line updates PATH for the Google Cloud SDK.
##if [ -f '/home/hk/sources/tarball/google-cloud-sdk/path.zsh.inc' ]; then . '/home/hk/sources/tarball/google-cloud-sdk/path.zsh.inc'; fi
##
### The next line enables shell command completion for gcloud.
##if [ -f '/home/hk/sources/tarball/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/hk/sources/tarball/google-cloud-sdk/completion.zsh.inc'; fi
##
##
### Set up Node Version Manager
##source /usr/share/nvm/init-nvm.sh
##
##
##
