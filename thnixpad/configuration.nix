# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

# This configuration file is a function that takes at least two arguments
# config and pkgs
{ config, pkgs, ... }:

# it returns a set of option definitions of the form
# name = value
# dots denote reverse element relationship
# boot.loader.x is the option x in the set loader
# which is contained in the set boot
# the // operator forms the union of two sets
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # enable proprietary software
  nixpkgs.config.allowUnfree = true; 

  # Use the systemd-boot EFI boot loader.
  boot = {
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
  };
 
  # Set your time zone.
  time.timeZone = "Europe/Zurich";

  networking = {
    hostName = "ThnixPad"; # Define your hostname.
    # NetworkManager is enabled by GNOME

    # The global useDHCP flag is deprecated, therefore explicitly set to false here.
    # Per-interface useDHCP will be mandatory in the future, so this generated config
    # replicates the default behaviour.
    useDHCP = false;
    interfaces.enp5s0.useDHCP = true;
    interfaces.wlp4s0.useDHCP = true;
    # Configure network proxy if necessary
    # proxy.default = "http://user:password@proxy:port/";
    # proxy.noProxy = "127.0.0.1,localhost,internal.domain";

    # Open ports in the firewall.
    # firewall.allowedTCPPorts = [ ... ];
    # firewall.allowedUDPPorts = [ ... ];
    # Or disable the firewall altogether.
    # firewall.enable = false;
  };




  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true;
  };
  fonts.fontconfig.dpi = 192;


  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;


  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.hk = {
    isNormalUser = true;
    extraGroups = [ "wheel" "audio" "video" "networkmanager" ]; 
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # alacritty
    # bspwm
    emacs
    firefox
    # git
    # htop
    # mpv
    # neovim
    # rofi
    # sxhkd
    # thunderbird
    vim 
    wget
    # youtube-dl
    # zathura
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # List services that you want to enable:
  services = {
    # Enable the OpenSSH daemon.
    # openssh.enable = true;
    # Enable the X11 windowing system.

    xserver = {
      enable = true;

      # Enable the GNOME Desktop Environment.
      displayManager.gdm.enable = true;
      desktopManager.gnome.enable = true;
      # Configure keymap in X11
      layout = "ch";
      # videoDrivers = [ "nvidia" ];
      
      # Enable touchpad support (enabled default in most desktopManager).
      libinput = {
        enable = true;
	# disable acceleration
        mouse.accelProfile = "flat";
        touchpad.accelProfile = "flat";
      }; 
    };

    # Enable CUPS to print documents.
    printing.enable = true;
  };



  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

}

