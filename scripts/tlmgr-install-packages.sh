tlmgr install \
  adjustbox \
  amscls \
  amsfonts \
  amsmath \
  algorithm2e   `# write algorithms` \
  arydshln      `# dashed lines` \
  babel-german \
  beamer        `# latex presentations` \
  braket        `# dirac notation` \
  caption       `# Caption on graphics` \
  capt-of       `# Required by Emacs Org mode` \
  collectbox \
  collection-fontsrecommended \
  collection-fontsextra \
  csquotes      `# context sensitive quotes` \
  doublestroke \
  environ       `# environment interface` \
  etoolbox \
  faktor \
  fancyhdr \
  float         `# better figure/table placement` \
  framed \
  geometry \
  graphics \
  hyperref \
  hyphen-german `# german hyphenation patterns` \
  hyphenat \
  hf-tikz       `# highlight formulas` \
  ifoddpage \
  import        `# file organisation` \
  jknapltx \
  listings      `# Formatted Code Blocks` \
  listingsutf8  `# UTF in listings`\
  makecell \
  mathdots \
  mathtools \
  mdframed      `# draw frames around boxes` \
  microtype     `# typographical refinements` \
  multirow      `# tabular cells over multiple rows` \
  neespace      `# automatic page breaks` \
  paracol       `# multi column text in parallel` \
  pgf           `# vector graphics package` \
  pgfopts \
  relsize       `# relative font sizes` \
  rsfs \
  sectsty \
  spreadtab     `# Spreadsheets` \
  stmaryrd      `# Math symbols` \
  tcolorbox     `# coloured boxes`\
  tensor \
  tikz-cd       `# commutative diagrams` \
  tools \
  ulem \
  was           `# upper case Greek` \
  wrapfi        `# wrap figures around text` \
  xkeyval \
  xstring \
  ytableau      `# young tableaus` \
  zref
