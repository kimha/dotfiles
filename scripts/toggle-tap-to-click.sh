#!/bin/bash

togglefile=$HOME/.local/state/tap-to-click/enabled

if [ ! -e $togglefile ]; then
  touch $togglefile
  gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click false
else
  rm $togglefile
  gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
fi

