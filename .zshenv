typeset -U PATH path

# PATH variables
export PATH"=$HOME/.local/bin:/usr/local/share/texlive/2022/bin/x86_64-linux:$PATH"
export MANPATH"=/usr/local/share/texlive/2022/texmf/doc/man:$MANPATH"
export INFOPATH"=/usr/local/share/texlive/2022/texmf/doc/info:$INFOPATH"
export PATH"=$HOME/.local/share/yarn/global/node_modules/.bin:$PATH"
export PATH"=$HOME/.cargo/bin:$PATH"
export PATH="$HOME/.pub-cache/bin:$PATH"
export PATH="$PATH:/home/hk/.dotnet/tools"


# XDG Base Directory
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_DIRS="$HOME/.nix-profile/share/:$XDG_DATA_DIRS"


# Coloured output
export LS_COLORS="di=1;34"
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;34m'

# GHCUP
[ -f "/home/hk/.ghcup/env" ] && source "/home/hk/.ghcup/env" # ghcup-env


# .NET
export DOTNET_CLI_TELEMETRY_OPTOUT=1


# NIX
export NIX_PATH="$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}"
