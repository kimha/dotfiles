#+title=Emacs Window Manager (EXWM)
#+author=Han-Miru Kim

* References
- [[https://github.com/ch11ng/exwm/wiki][EXWM wiki]]
- [[https://systemcrafters.cc/emacs-desktop-environment/][Systemcrafters Tutorial]]

  
* Installation
** Prerequisites
Install emacs, git and fonts
#+begin_src sh
sudo pacman -S emacs git ttf-dejavu noto-fonts
#+end_src

Write a startup script. 

Create .desktop file for the display manager at
~/usr/share/xsessions/exwm.desktop~

#+begin_src sh
[Desktop Entry]
Name=EXWM
Comment=Emacs Window Manager
Exec=sh /home/hk/.emacs.d/exwm/start-exwm.sh
TryExec=sh
Type=Application
#+end_src

And in the file =start-exwm.sh=, write

#+begin_src sh
  #!/bin/sh
  exec dbus-launch --exit-with-session emacs -mm --debug-init
#+end_src
and make it executable
#+begin_src sh
  sudo chmod 666 start-exwm.sh
#+end_src
Then create a symlink to set your display manager see exwm as an option.


* Window Management
** EXWM Configuration
#+begin_src emacs-lisp
  (use-package exwm
    :config
    (setq exwm-workspace-number 9)
    (exwm-enable))
#+end_src


