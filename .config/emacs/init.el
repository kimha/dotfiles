(global-set-key (kbd "<escape>") 'keyboard-escape-quit) ;; Esc to quit
(global-set-key (kbd "C-x C-e") 'dired)
(global-set-key (kbd "C-x C-v") 'eval-region)

;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time
;; scrolling
;;(setq redisplay-dont-pause t
;;scroll-margin 1
;;scroll-step 1
;;scroll-conservatively 10000
;;scroll-preserve-screen-position 1)
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key (kbd "C-x C-r") 'recentf-open-files)

(setq inhibit-startup-message t) ;; Disable greeting screen
(setq vc-follow-symlinks t)  ;; follow symlinks by default
(setq custom-file "~/.config/emacs/custom.el") ;; move easy custom file
(load-file custom-file)

;; TABS

(scroll-bar-mode -1) ;; Disable scrollbar
(tool-bar-mode -1 )  ;; Disable toolbar
(menu-bar-mode -1)   ;; Disable menu bar
;;(column-number-mode)
(global-display-line-numbers-mode -1)
;;(defun linum-format-func (line)
;;(let ((w (length (number-to-string (count-lines (point-min) (point-max))))))
;    (propertize (format (format "%%%dd " w) line) 'face 'linum)))
;;(setq linum-format 'linum-format-func)
(setq display-line-numbers 'relative)

;; Default font
(set-face-attribute 'default nil
                    :font "DejaVu Sans Mono Nerd Font"
                    :height 128)
;; Set the fixed pitch face
(set-face-attribute 'fixed-pitch nil
                    :font "DejaVu Sans Mono Nerd Font"
                    :height 128)

;; Set the variable pitch face
(set-face-attribute 'variable-pitch nil
                    ;;:font "Noto Sans"
                    :font "DejaVu Sans Mono Nerd Font"
                    :height 128)

;; Fix emacs daemon font issue
(defun set-font-faces ()
  (message "Setting faces!")
  (set-face-attribute 'default nil :font "DejaVu Sans Mono Nerd Font" :height 128)

  ;; Set the fixed pitch face
  (set-face-attribute 'fixed-pitch nil :font "DejaVu Sans Mono Nerd Font" :height 128)

  ;; Set the variable pitch face
  (set-face-attribute 'variable-pitch nil :font "DejaVu Sans Mono Nerd Font" :height 128 :weight 'regular))

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (setq doom-modeline-icon t)
                (with-selected-frame frame
                  (set-font-faces))))
    (set-font-faces))

(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org"   . "https://orgmode.org/elpa")
                         ("elpa"  . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; use-package
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(use-package dashboard
  :ensure t
  :diminish dashboard-mode
  :config
  (setq dashboard-banner-logo-title "Welcome to Emacs!")
  ;;(setq dashboard-startup-banner "/path/to/image")
  (setq dashboard-items '((bookmarks . 10)
                          (recents . 5)))
  (dashboard-setup-startup-hook))
(setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))
(use-package doom-modeline ;; M-x all-the-icons-install-fonts
  :ensure t
  :init (doom-modeline-mode 1))
(use-package doom-themes
  :ensure t
  :config
  (load-theme 'doom-tomorrow-night t))
(setq doom-modeline-buffer-encoding nil)

(use-package command-log-mode) ;; C-c o
(use-package counsel)          ;; Command autocomplete
(use-package haskell-mode)     ;; haskell minor mode
(use-package lua-mode)         ;; lua major mode
(use-package yaml-mode)        ;; yaml major mode
(use-package powershell)       ;; powershell minor mode
;;(use-package openwith         ;; ope files with xdg-open
  ;;:init (openwith-mode)
  ;;;:diminish openwith-mode
  ;;:config
  ;;(setq openwith-associations"\\.pdf\\'" "xdg-open" (file)))
;;(use-package dart-mode)        ;; dart major mode
;;(use-package geiser)           ;; 
(use-package geiser-guile)     ;; guile
(use-package which-key         ;; display available keybindings
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.5))

(use-package projectile        
   :diminish projectile-mode
   :config (projectile-mode)
   :bind-keymap
   ("C-c p" . projectile-command-map)
   :init
   (when (file-directory-p "~/projects")
     (setq projectile-project-search-path '("~/projects")))
   (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;(use-package evil-magit
;  :after magit)

(use-package ivy               ;; Autocomplete
    :diminish                      ;; hide mode-name
    :bind (("C-s" . swiper)
           :map ivy-minibuffer-map
           ("TAB" . ivy-alt-done)
           ("C-l" . ivy-alt-done)
           ("C-j" . ivy-next-line)
           ("C-k" . ivy-previous-line)
           :map ivy-switch-buffer-map
           ("C-k" . ivy-previous-line)
           ("C-l" . ivy-done)
           ("C-d" . ivy-switch-buffer-kill)
           :map ivy-reverse-i-search-map
           ("C-k" . ivy-previous-line)
           ("C-d" . ivy-reverse-i-search-kill))
    :config
    (ivy-mode 1))

(setq ivy-count-format "(%d/%d) ")
(setq ivy-use-virtual-buffers t)
(setq swiper-use-visual-line nil)
(setq swiper-use-visual-line-p (lambda (a) nil))
(global-set-key (kbd "C-x C-f") 'counsel-find-file)

(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1)
  :config
  (add-to-list 'yas-snippet-dirs (locate-user-emacs-file "snippets")))
;; Jump forward and backward
(define-key yas-keymap (kbd "M-j") 'yas-next-field-or-maybe-expand)
(define-key yas-keymap (kbd "M-k") 'yas-prev-field)

(defun my-yas-try-expanding-auto-snippets ()
  (when yas-minor-mode
    (let ((yas-buffer-local-condition ''(require-snippet-condition . auto)))
      (yas-expand))))
(add-hook 'post-command-hook #'my-yas-try-expanding-auto-snippets)

(setq evil-want-C-i-jump nil) 
(use-package evil)
  (evil-mode 1)
;; Change write and quit 
(define-key evil-window-map "q" 'kill-this-buffer)
(evil-ex-define-cmd "q[uit]" 'kill-this-buffer)
(evil-ex-define-cmd "wq" 'write-and-kill-buffer)
(define-key evil-normal-state-map "ZZ" 'write-and-kill-buffer)
(define-key evil-normal-state-map "ZQ" 'kill-this-buffer)
(defun write-and-kill-buffer ()
  "Run `evil-write` and `kill-this-buffer` in sequence."
  (interactive)
  (call-interactively 'evil-write)
  (call-interactively 'kill-this-buffer))

;; Unbind C-. and M-.
(define-key evil-normal-state-map (kbd "C-.") nil)
(define-key evil-normal-state-map (kbd "M-.") nil)

;; == Org Mode ==
(defun my/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (auto-fill-mode 0)
  (visual-line-mode 1)) ;; enable word-wrap and visual line editing

(use-package org
  :hook (org-mode . my/org-mode-setup)
  :config
  (setq org-agenda-files '("~/org/tasks.org"
                           "~/org/calendar.org"
                           "~/org/birthdays.org")))

;;(use-package org-bullets
  ;;:after org
  ;;:hook (org-mode . org-bullets-mode)
  ;;:custom
  ;;(org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "
;;●")))

;;(use-package org-appear)
;;(add-hook 'org-mode-hook 'org-appear-mode)
(defun org-toggle-emphasis ()
  "Toggle hiding/showing of org emphasize markers."
  (interactive)
  (if org-hide-emphasis-markers
      (set-variable 'org-hide-emphasis-markers nil)
    (set-variable 'org-hide-emphasis-markers t))
  (org-mode-restart))

(require 'org-faces)
(dolist (face '((org-level-1 . 1.2)
                (org-level-2 . 1.15)
                (org-level-3 . 1.1)
                (org-level-4 . 1.05))))

;; Ensure corrent fonts for org environemnts
(set-face-attribute 'org-table    nil :inherit 'fixed-pitch)
(set-face-attribute 'org-code     nil :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-block    nil :foreground nil :inherit 'fixed-pitch)
(set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))

;; Add padding to org mode
;;(defun my/org-mode-visual-fill ()
 ;;(setq visual-fill-column-width 100
       ;;visual-fill-column-center-text t)
 ;;(visual-fill-column-mode 1))

;;(use-package visual-fill-column
  ;;:hook (org-mode . my/org-mode-visual-fill))

(with-eval-after-load 'ox-latex
(add-to-list 'org-latex-classes
             '("org-plain-latex"
               "\\documentclass{article}
           [NO-DEFAULT-PACKAGES]
           [PACKAGES]
           [EXTRA]"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))


(with-eval-after-load 'ox-latex
(add-to-list 'org-latex-classes
             '("org-plain-beamer"
               "\\documentclass{beamer}
           [NO-DEFAULT-PACKAGES]
           [PACKAGES]
           [EXTRA]"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(use-package hide-mode-line)

(defun my/presentation-setup ()
  (setq text-scale-mode-amount 2)
  (hide-mode-line-mode 1)
  (org-display-inline-images)
  (text-scale-mode 1))
(defun my/presentation-end ()
  (hide-mode-line-mode 0)
  (text-scale-mode 0))

(use-package org-tree-slide
  :hook ((org-tree-slide-play . my/presentation-setup)
         (org-tree-slide-stop . my/presentation-end))
  :custom
  (org-image-actual-width nil))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((C . t)
   (python . t)
   (haskell . t)
   (lua . t)
   ;;(latex-macros . t)
   ;;(latex-macros-inline . t)
   (powershell . t)
   (shell . t)))

(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/roam/")
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert))
  :config
  (org-roam-setup))

(eval-after-load 'org
  '(define-key org-mode-map (kbd "C-c e") 'org-toggle-emphasis))
(eval-after-load 'org
  '(define-key org-mode-map (kbd "C-. C-l") 'org-export-dispatch))
(eval-after-load 'org
  '(define-key org-mode-map (kbd "C-. C-t") 'org-babel-tangle))
(eval-after-load 'org
  '(define-key org-mode-map (kbd "C-. C-p") 'org-tree-slide-mode))
