vim.g.tex_flavor = 'lualatex'
vim.g.vimtex_view_general_viewer = 'zathura'

vim.g.vimtex_compiler_method = 'latexrun'
