set runtimepath^=/home/hk/.config/vim 
set runtimepath+=/home/hk/.config/vim/after 
"set runtimepath+=/home/hk/.config/nvim/autoload
let &packpath = &runtimepath
set viminfo+=n$HOME/.config/vim/.viminfo

set encoding=utf-8
set nocompatible " use vim, over vi
set hidden " keeps bufers alive when abandoned, acces with :ls and :bN for N number
let g:python3_host_prog = '/usr/bin/python3' " set python3 provider

filetype plugin indent on
syntax on
set mouse=a

autocmd BufRead,BufNewFile *.rasi setf css
autocmd BufRead,BufNewFile config* setf dosini
autocmd BufRead,BufNewFile *.dart setf dart

set hlsearch        " highlight search
set incsearch       " incremental search
set showmatch

set tabstop=2       " tab size
set shiftwidth=2    " autoindent size
set softtabstop=2   " let <BS> delete multiple spaces
set expandtab       " expand tabs into spaces
set autoindent      " indent newline

set number relativenumber
set statusline=%F%m%r%h%w\ [POS=%04l,%04v]\ [%p%%]

call plug#begin('$HOME/.config/vim/plugged')
"Plug 'justinmk/vim-sneak'
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'SirVer/ultisnips'
Plug 'mhinz/vim-startify'
Plug 'theprimeagen/vim-be-good'
Plug 'lervag/vimtex'
Plug 'dense-analysis/ale'
Plug 'dart-lang/dart-vim-plugin'
Plug 'tasn/vim-tsx'
"Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'derekelkins/agda-vim'
Plug 'joshdick/onedark.vim'
Plug 'junegunn/goyo.vim'
Plug 'ap/vim-css-color'
Plug 'chentau/marks.nvim'
Plug 'LnL7/vim-nix'
call plug#end()

let g:tex_flavor='latex'
let g:vimtex_quickfix_mode=0
let g:vimtex_mathparen_enabled=1

let g:vimtex_compiler_latexmk = {
  \ 'options' : [
    \ '-pdf',
    \ '-pvc',
    \ '-shell-escape',
    \ '-synctex=1',
    \ '-interaction=nonstopmode',
  \ ],
\}

let g:vimtex_view_method='zathura'
let g:vimtex_view_general_options = '-reuse-instance -forward-search @tex @line @pdf'
"let g:vimtex_view_general_options_latexmk = '-reuse-instance'

nnoremap <C-t>      : NERDTreeToggle<CR>
nnoremap <C-f>      : NERDTreeFind<CR>
nnoremap <leader>n  : NERDTreeFocus<CR>

let g:UltiSnipsSnippetDirectories=[expand('$HOME/dotfiles/.config/vim/UltiSnips')]

let g:UltiSnipsExpandTrigger='<tab>'
let g:UltiSnipsJumpForwardTrigger='<tab>'
let g:UltiSnipsJumpBackwardTrigger='<C-k>'
let g:UltiSnipsEditSplit='vertical'
inoremap <C-j> <C-o>:call UltiSnips#JumpForwards()<CR>

nnoremap <localleader>ht :GhcModType<cr>
nnoremap <localleader>htc :GhcModTypeClear<cr>

let g:ale_linters ={
      \   'haskell': ['hlint', 'hdevtools', 'hfmt'],
      \   'python': ['unimport', 'mypy', 'autoflake', 'reorder-python-imports'],
      \}

let g:startify_custom_header = []
let g:startify_bookmarks = [
      \ { 'z': '~/.zshrc' },
      \ { 'a': '~/.config/zsh/aliases' },
      \]

let g:startify_lists = [
      \ { 'header': ['Bookmarks'], 'type': 'bookmarks' },
      \ { 'header': ['MRU'], 'type': 'files' },
      \ { 'header': ['MRU', getcwd()], 'type': 'dir' },
      \]

nmap <C-n> :Startify<CR>

inoremap <silent><expr> <c-space> coc#refresh()
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

autocmd FileType python map <buffer> <F9> :w<CR>:exec '!python3' shellescape(@%, 1)<CR>

let mapleader = ","
let maplocalleader = " "

inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

nnoremap <A-S-h> <C-W>H
nnoremap <A-S-j> <C-W>J
nnoremap <A-S-k> <C-W>K
nnoremap <A-S-l> <C-W>L



