" Basic Settings
set encoding=utf-8
set nocompatible " use vim, over vi
set hidden " keeps bufers alive when abandoned, acces with :ls and :bN for N number
set viminfo+=n$HOME/.config/vim/.viminfo

filetype plugin indent on
syntax on
set mouse=a

" search
set hlsearch        " highlight search
set incsearch       " incremental search
set showmatch

" tab
set tabstop=2       " tab size
set shiftwidth=2    " autoindent size
set softtabstop=2   " let <BS> delete multiple spaces
set expandtab       " expand tabs into spaces
set autoindent      " indent newline

" Appearance
set number relativenumber " enables relative line numbers
set statusline=%F%m%r%h%w\ [POS=%04l,%04v]\ [%p%%]

